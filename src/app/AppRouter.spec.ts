/* tslint:disable:no-unused-variable */
import {TestBed, fakeAsync, tick} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './components/app.component';
import {PlaylistService} from './services/playlist.service';
import {PlayerService} from './services/player.service';
import {AudioService} from './services/audio.service';
import {AppModule} from './app.module';
import {routes} from './visualizer/visualizer.routing';
import {Router, RouterModule} from '@angular/router';
import {Location} from '@angular/common';
import {VisualizerModule} from './visualizer/visualizer.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {VisualizerComponent} from './visualizer/visualizer.component';


describe('Router: App', () => {

  let location: Location;
  let router: Router;
  let fixture;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        AppModule,
        VisualizerModule,
        BrowserAnimationsModule,
      ],
      providers: [
        PlaylistService,
        PlayerService,
        AudioService
      ],
    });

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(AppComponent);
    router.initialNavigation();
  });

  // it('fakeAsync works', fakeAsync(() => {
  //   const promise = new Promise((resolve) => {
  //     setTimeout(resolve, 10);
  //   });
  //   let done = false;
  //   promise.then(() => done = true);
  //   tick(50);
  //   expect(done).toBeTruthy();
  // }));
  //
  // it('navigate to "/visualizer" redirects you to /visualizer/hopalong', fakeAsync(() => {
  //   router.navigate([{ outlets: { visualization: ['visualizer'] } }]).then(() => {
  //     expect(location.path()).toBe('/(visualization:visualizer/hopalong)');
  //   });
  // }));

});
