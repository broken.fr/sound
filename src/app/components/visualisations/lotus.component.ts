import {Component} from '@angular/core';

import {HedgehogComponent} from './hedgehog.component';
@Component({
  templateUrl: '../../views/visualisations/generic.component.html',
})
export class LotusComponent extends HedgehogComponent {
  vertexChoiceStep = 1;
}
