import {Component, Renderer2} from '@angular/core';
import { AbstractWebglViewComponent } from './AbstractWebglView.abstract';
// import * as TWEEN from 'tween.js';
// declare let TWEEN: any;
declare let SPARKS: any;

import * as THREE from 'three-full';
import {AudioService} from '../../services/audio.service';
import {Router} from '@angular/router';
import {PlayerService} from '../../services/player.service';
import {PlaylistService} from '../../services/playlist.service';
declare let THREEx: any;

export class BeatCounter {
  cache;
  at;
  count;
  done;
  result;
  length;
  constructor(length) {
    this.cache = {}, this.at = 0, this.count = 0, this.done = false, this.result = {};
    this.length = length;
  }
  tick () {
    if (this.done && this.cache[this.at]) {
        this.count--;
    }
    this.at = (this.at + 1) % this.length;
    this.done = this.done || (this.at === 0);
  };
  percentage() {
    if (!this.done) {
        return this.count / this.at;
    }
    return this.count / this.length;
  };
}

export class RythmCounter {
  leftover = 0;
  rate = 10;
  beat_counter: BeatCounter;
  constructor(beat_counter: BeatCounter) {
    this.beat_counter = beat_counter;
  }
  updateEmitter(emitter, time) {
    let rate = this.beat_counter.percentage() * 80 + 3;
    const targetRelease = time * rate + this.leftover;
    const actualRelease = Math.floor(targetRelease);
    this.leftover = targetRelease - actualRelease;
    return actualRelease;
  }
}

@Component({
    templateUrl: '../../views/visualisations/particles.component.html',
})
export class ParticlesComponent extends AbstractWebglViewComponent {
    // @TODO : https://threejs.org/examples/#webgl_buffergeometry_instancing_billboards
  protected composer: THREE.EffectComposer;
  protected cube: THREE.Mesh;
  protected pointLight;
  protected particleCloud;
  protected attributes;
  protected uniforms;
  protected particles;
  protected Pool;
  protected material;
  protected beat_counter;
  protected hue;
  protected particlesDetails = {
    count: 75000
  };
  protected shaders = {
    vertex: [
      'uniform float time;',
      'uniform vec2 resolution;',
      'void main()	{',
        'gl_Position = vec4( position, 1.0 );',
      '}'
    ].join('\n'),
    fragment: [
      'uniform float time;',
      'uniform vec2 resolution;',
      'void main()	{',
      'float x = mod(time + gl_FragCoord.x, 20.) < 10. ? 1. : 0.;',
      'float y = mod(time + gl_FragCoord.y, 20.) < 10. ? 1. : 0.;',
      'gl_FragColor = vec4(vec3(min(x, y)), 1.);',
      '}'
      ].join('\n')
  };
  protected shaderExtras = {
    'screen': {
      uniforms: {
        tDiffuse: {type: 't', value: 0, texture: null},
        opacity: {type: 'f', value: 1.0}
      },
      vertexShader: [
        'varying vec2 vUv;',
        'void main() {',
        'vUv = vec2( uv.x, 1.0 - uv.y );',
        'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
        '}'
      ].join('\n'),
      fragmentShader: [
        'uniform float opacity;',
        'uniform sampler2D tDiffuse;',
        'varying vec2 vUv;',
        'void main() {',
        'vec4 texel = texture2D( tDiffuse, vUv );',
        'gl_FragColor = opacity * texel;',
        '}'
      ].join('\n')
    }
  };

  addInitializer(emitter, init_func) {
    emitter.addInitializer(this.updaterObject(init_func, 'initialize'));
  };

  addAction(emitter, init_func) {
    emitter.addAction(this.updaterObject(init_func, 'update'));
  };

  drift(base, distance) {
    const r = Math.random() * 2 - 1;
    return base + Math.floor(r * distance);
  };


  updaterObject(updater_func, update_type) {
    const Obj = () => {
      this[update_type] = updater_func;
    };
    return new Obj();
  };

  generateSprite() {
    const canvas = document.createElement('canvas');
    canvas.width = 128;
    canvas.height = 128;

    const context = canvas.getContext('2d');
    context.beginPath();
    context.arc(64, 64, 60, 0, Math.PI * 2, false);

    context.lineWidth = 0.5; // 0.05
    context.stroke();
    context.restore();

    const gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0,
      canvas.width / 2, canvas.height / 2, canvas.width / 2);

    gradient.addColorStop(0, 'rgba(255,255,255,1)');
    gradient.addColorStop(0.2, 'rgba(255,255,255,1)');
    gradient.addColorStop(0.4, 'rgba(200,200,200,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,1)');

    context.fillStyle = gradient;

    context.fill();

    return canvas;
  }

  poolCreator() {
    return {
      __pools: [],
      // Get a new Vector
      get: function () {
        if (this.__pools.length > 0) {
          return this.__pools.pop();
        }
        console.error('pool ran out!');
      },
      // Release a vector back into the pool
      add: function (v) {
        this.__pools.push(v);
      }
    };
  };

  prepareParticleGeometry(particle_count) {
    // sets up three.js side of things, the passing of values to shader.
    const particles = new THREE.Geometry();
    const Pool = this.poolCreator();
    for (let i = 0; i < particle_count; i++) {
      const vertex = new THREE.Vector3();
      vertex.x = Math.random() * 2000 - 1000;
      vertex.y = Math.random() * 2000 - 1000;
      vertex.z = Math.random() * 2000 - 1000;
      particles.vertices.push(vertex); // TODO: set to new THREE.Vector3(0, 0, 0) if you don't want to see them at init
      Pool.add(i);
    }

    const attributes = {
      size: {type: 'f', value: []},
      pcolor: {type: 'c', value: []}
    };

    const sprite = this.generateSprite();

    const texture = new THREE.Texture(sprite);
    texture.needsUpdate = true;

    const uniforms = {
      texture: {type: 't', value: texture},
      time: { type: "f", value: 1.0 },
      resolution: { type: "v2", value: new THREE.Vector2() }
    };
    const shaderMaterial = new THREE.ShaderMaterial({
      uniforms: uniforms,
      vertexShader: this.shaders.vertex,
      fragmentShader: this.shaders.fragment,
      blending: THREE.AdditiveBlending,
      depthWrite: false,
      transparent: true
    });
    shaderMaterial.defaultAttributeValues = attributes; // TODO: Look what it does (should replace "attributes")

    const particleCloud = new THREE.Points(particles, shaderMaterial);

    // particleCloud.dynamic = true; // @TODO: JE SAIS PAS CE QUE CA FAIT !

    const vertices = particleCloud.geometry.toJSON().data.vertices;
    const values_size = attributes.size.value;
    const values_color = attributes.pcolor.value;
    for (let v = 0; v < vertices.length; v++) {
      values_size[v] = 50;
      values_color[v] = new THREE.Color(0x000000);
      if (particles.vertices[v]) {
        particles.vertices[v].set(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);
      }
    }

    return {
      attributes: attributes,
      uniforms: uniforms,
      particleCloud: particleCloud,
      particles: particles,
      Pool: Pool,
      shaderMaterial: shaderMaterial
    };
  }

  public initEmitters() {
    const values_size = this.attributes.size.value;
    const values_color = this.attributes.pcolor.value;

    this.hue = 0;

    const setTargetParticle = () => {
      const target = this.Pool.get();
      values_size[target] = Math.random() * 2000 + 100;
      return target;
    };

    const onParticleCreated = (p) => {
      // const position = p.position;
      // console.log(position);
      // p.target.position = position;
      const target = p.target;

      if (target) {
        this.hue += 0.009;
        if (this.hue > 1) {
          this.hue -= 1;
        }

        this.particles.vertices[target] = p.position;

        values_color[target].setHSL(this.hue, 0.6, 0.1);
      }
    };

    const onParticleDead = (particle) => {


      const target = particle.target;
      if (target) {
        // Hide the particle
        this.attributes.pcolor.value[target].setRGB(0, 0, 0);
        this.particles.vertices[target].set(Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY);
        // Mark particle system as available by returning to pool
        this.Pool.add(particle.target);
      }
    };
    const emitter_position = new THREE.Vector3(0, 0, 0);
    const threexSparks	= new THREEx.Sparks({
      maxParticles	: 400,
      counter		: new SPARKS.SteadyCounter(300)
    });
    const emitter = new SPARKS.Emitter((new RythmCounter(this.beat_counter)));
    this.addInitializer(emitter, (emitter, particle) => {

      const pos = emitter_position;
      const x = this.drift(pos.x, 100),
        y = this.drift(pos.y, 100),
        z = this.drift(pos.z, 100);
      particle.position.set(x, y, z);
    });

    emitter.addInitializer(new SPARKS.Lifetime(3, 10));
    emitter.addInitializer(new SPARKS.Target(null, setTargetParticle));

    this.addInitializer(emitter, (e, particle) => {
      // const d = this.beatDetector.is_beat ? 200 : 200;
      // values_size[particle.target] = Math.random() * d + 100;
      const drift = new THREE.Vector3(1000, 1000, 1000);
      particle.acceleration = new THREE.Vector3(
        2 * (Math.random() - 0.5), 2 * (Math.random() - 0.5), 2 * (Math.random() - 0.5)
      );
      particle.acceleration.multiply(drift);
    });


    emitter.addInitializer(new SPARKS.Velocity(
      new SPARKS.PointZone(new THREE.Vector3(0, -5, 1))));
    // TOTRY Set velocity to move away from centroid

    emitter.addAction(new SPARKS.Age());
    this.addAction(emitter, (e, particle, time) => {
      const acc = particle.acceleration;

      const v = particle.velocity;

      particle._oldvelocity.set(v.x, v.y, v.z);

      v.x += acc.x * time;
      v.y += acc.y * time;
      v.z += acc.z * time;
    });
    // emitter.addAction( new SPARKS.Accelerate( 0, 0, 50 ) );
    emitter.addAction(new SPARKS.Move());
    this.addAction(emitter, (e, particle, time) => {
      const p = this.beat_counter.percentage(),
        v = particle.velocity,
        new_speed = Math.pow(p, 0.8) * 3000 + 100;
      v.setLength(new_speed);
    });
    emitter.addAction(new SPARKS.RandomDrift(100, 100, 100));

    emitter.addCallback('created', onParticleCreated);
    emitter.addCallback('dead', onParticleDead);
    emitter.start();
  };

  scene_init(renderer) {

    // let self: ParticlesComponent = this;
    const view = {
      angle: 75,
      aspect: window.innerWidth / window.innerHeight,
      near: 1,
      far: 3000
    };
    const cameraZ = view.far / 3;
    this.beat_counter = new BeatCounter(60);
    this.scene = new THREE.Scene();
    renderer.setClearColor(0x000000);
    this.camera = new THREE.PerspectiveCamera(view.angle, view.aspect, view.near, view.far);
    this.camera.position.z = cameraZ;
    this.camera.lookAt(this.scene.position);
    renderer.setClearColor(0x000000);

    // transparently support window resize
    // THREEx.WindowResize.bind(renderer, camera);

    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    directionalLight.position.set(0, -1, 1);
    directionalLight.position.normalize();
    this.scene.add(directionalLight);

    this.pointLight = new THREE.PointLight(0xffffff, 2, 300);
    this.pointLight.position.set(0, 0, 0);
    this.scene.add(this.pointLight);
    const a = this.prepareParticleGeometry(this.particlesDetails.count);
    this.particleCloud = a.particleCloud;
    this.particleCloud.y = 800;
    this.attributes = a.attributes;
    this.uniforms = a.uniforms;
    this.particles = a.particles;
    this.Pool = a.Pool;
    this.material = a.shaderMaterial;
    console.log(this.particleCloud);

    this.scene.add(this.particleCloud);
    this.initEmitters();

    this.composer = new THREE.EffectComposer( renderer );
    renderer.autoClear = false;
    this.composer.addPass( new THREE.RenderPass( this.scene, this.camera ) );


    const effectBloom = new THREE.BloomPass(1.5);
    this.composer.addPass(effectBloom);

    const effectScreen = new THREE.ShaderPass(this.shaderExtras[ 'screen' ]);
    effectScreen.renderToScreen = true;
    this.composer.addPass(effectScreen);
    let effect;
    effect = new THREE.ShaderPass( THREE.DotScreenShader );
    effect.uniforms[ 'scale' ].value = 7;
    this.composer.addPass( effect );


    effect = new THREE.ShaderPass( THREE.RGBShiftShader );
    effect.uniforms[ 'amount' ].value = 0.0015;
    effect.renderToScreen = true;
    this.composer.addPass( effect );

  };

  render(renderer, time_delta) {
    this.beat_counter.tick();
    this.attributes.size.needsUpdate = true;
    this.attributes.pcolor.needsUpdate = true;
    renderer.clear();
    this.composer.render();
  }
}
