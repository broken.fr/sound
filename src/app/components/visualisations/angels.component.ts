import {Component} from '@angular/core';
import {HedgehogComponent} from './hedgehog.component';
@Component({
  templateUrl: '../../views/visualisations/generic.component.html',
})
export class AngelsComponent extends HedgehogComponent {
  vertexChoiceStep = 4;
}
