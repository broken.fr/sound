import { Component, Input, Output } from '@angular/core';
@Component({
    selector: '[folder]',
    templateUrl: '../views/folder.component.html',
})
export class FolderComponent {
    @Input() title: string;
    open;
}
