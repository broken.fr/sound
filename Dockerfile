FROM node:18-alpine as build-step
#RUN node --version
#RUN npm -v
RUN apk add --no-cache python3 g++ make
#FROM node:latest as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json package-lock.json /app/

#RUN yarn global add @angular/cli --non-interactive --network-timeout 1000000
#RUN ng config -g cli.packageManager yarn
#RUN yarn install --frozen-lockfile --network-timeout 1000000
RUN npm install -timeout=8000000
COPY . /app
#RUN yarn run build --configuration production
RUN npm run ng build --configuration=production
FROM nginx:1.21.1-alpine
#FROM nginx:1.21.1-alpine
COPY --from=build-step /app/dist /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/conf.d/default.conf
EXPOSE 80
