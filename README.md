[![pipeline status](https://gitlab.com/broken.fr/sound/badges/update/pipeline.svg)](https://gitlab.com/broken.fr/sound/commits/master)
[![coverage report](https://gitlab.com/broken.fr/sound/badges/update/coverage.svg)](https://gitlab.com/broken.fr/sound/)

# AudioViz

This project was originaly generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--configuration=production` flag for a production build.
Run `ng build --configuration=production --aot=false` If the build fails
## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




## Deployment

### Docker
```sh
docker build -t sound  --build-arg prod="true" .
docker run -d -it --rm --name sound -p 80:80 registry.gitlab.com/broken.fr/sound/master:latest
```
### Kubernetes

> File namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: sound
  labels:
    name: sound
```
> File sound-builded.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sound-builded
spec:
  replicas: 1
  selector:
    matchLabels:
      role: sound-builded
      environment: test
  template:
    metadata:
      labels:
        role: sound-builded
        environment: test
    spec:
      containers:
      - name: sound
        image: registry.gitlab.com/broken.fr/sound/master:latest
        imagePullPolicy: Always
        ports:
        - containerPort: 80
      imagePullSecrets:
      - name: regcred
```
```sh
kubectl create -f namespace.yaml
kubectl config set-context --current --namespace=sound
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com/broken.fr/sound --docker-username=<secret_registry_token> --docker-password=<secret_registry_password>
kubectl create -f sound-builded.yaml
kubectl expose deployment sound-builded --type=NodePort --port 80 -n sound
minikube service sound -n sound-builded
```

## Development

### Build 
```sh
docker build -f "dev.Dockerfile" -t sound .
```
### Linux 
```sh
docker run -dit --rm --name sound -v $PWD:/app -p 4200:4200 registry.gitlab.com/broken.fr/sound/master:master-dev
```

### Windows 
```sh
docker run -it --rm --name sound -v %cd%:/app -p 4200:4200 registry.gitlab.com/broken.fr/sound/master:master-dev /bin/bash
```

### Kubernetes
I'm using minikube, here are the files to deploy the application : 

```sh
sudo minikube start --vm-driver=none --apiserver-ips 127.0.0.1 --apiserver-name localhost
sudo chown -R $USER $HOME/.minikube                                                      
sudo chgrp -R $USER $HOME/.minikube
```
> File namespace.yaml
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: sound
  labels:
    name: sound
```
> File sound.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sound
spec:
  replicas: 1
  selector:
    matchLabels:
      role: sound
      environment: test
  template:
    metadata:
      labels:
        role: sound
        environment: test
    spec:
      containers:
      - name: sound
        image: registry.gitlab.com/broken.fr/sound/master:master-dev
        imagePullPolicy: Always
        args:
        - bash
        stdin: true
        stdinOnce: true
        tty: false
        workingDir: "/app"
        ports:
        - containerPort: 4200
        volumeMounts:
        - mountPath: /app
          name: volume
      imagePullSecrets:
      - name: regcred
      volumes:
      - name: volume
        hostPath:
            path: "<path/to/sound>"
            type: Directory
```

```sh
kubectl create -f namespace.yaml
kubectl config set-context --current --namespace=sound
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com/broken.fr/sound --docker-username=<secret_registry_token> --docker-password=<secret_registry_password>
kubectl create -f sound.yaml
kubectl expose deployment sound --type=NodePort --port 4200 -n sound
minikube service sound -n sound
```
