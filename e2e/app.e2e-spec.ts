import { AudioVizPage } from './app.po';

describe('audio-viz App', () => {
  let page: AudioVizPage;

  beforeEach(() => {
    page = new AudioVizPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('AudioViz');
  });
});
