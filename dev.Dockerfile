FROM node:13.8
VOLUME  ["/app"]
WORKDIR /app
RUN yarn global add @angular/cli --non-interactive
RUN ng config -g cli.packageManager yarn
EXPOSE 4200
CMD yarn install --non-interactive && yarn run start
